﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace List
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) //vorm load aktiveeritakse siis, kui vorm laaditakse ekraanile
        {
            NorthwindEntities db = new NorthwindEntities(); //teeme uue liikme klassi Northwindentities
            this.listBox1.DataSource = db.Categories
                            .Select(x => x.CategoryName)
                            .ToList()
                            ;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string categoryName = ((ListBox)sender).SelectedItem.ToString(); //castingu tehe ((ListBox)sender)
            NorthwindEntities db = new NorthwindEntities(); //teeme uue liikme klassi Northwindentities
            this.dataGridView1.DataSource = //alumised andmed paneme siia datasource sisse nüüd
            db.Categories.Where(x => x.CategoryName == categoryName)//where annab mitu asja, list kategoorima jatema elemen kategooria nimi peavad langema kokku. Me tahame ühte kategooriat ainult, seega tuleb single.
            ////siin peaks tuleb üks ainult (kui on palju, siis tuleb kasutada first, või single and default või first anddefault)
                .Single().Products.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })//sELECT - SISSE TULEVAD PRODUCTID ja välja lähevad product ID, product bame ja unit price
                //UnitPrice = x.UnitPrice?.ToString("F2") 
                // KUI TAHAD TEHA KAHE KOHAKOHAGA. sEE ? ANNAB SELLE, ET KUI POLE NULLABLE OBJEKTIL (SELLEL HINNAL) VÄÄRTUST, SIIS ON KANDILIEN NULL JA KUI ON VÄÄRTUS, SIIS SAAB SELLE ÜMARDATUD VÄÄRTUSE.
                .ToList()
                ;
            dataGridView1.Columns[2].DefaultCellStyle.Format = "F2";  //NIIMOODI SAAB NÄIDATA SEDA NUMBRIT KAHE KOMA KOHAGA, AGA TA EI MUUDA SEDA MUUTUJAT ENNAST, LIHTSALT TEMA NÄITAMISE VIISI
            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; //SAAME KA REANDADA SINNA KUHU SOOVID TERVE TABLEI, KUI COLOUMNI ÄRA JÄTAD
        }
    }
}
